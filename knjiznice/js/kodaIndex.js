var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/* global $, btoa*/

var ehrIDs = [];
var names = ["Pujsek", "Nosorog", "Jastreb"];
var surnames = ["Peter", "Nace", "Janez"];
var datesOfBirth = ["2000-02-02", "1999-05-05", "1924-06-06"];

var visine = [[180, 182, 185], [177, 179, 178], [155, 163, 170]];
var teže = [[70, 72, 78], [81, 83, 75], [56, 60, 58]];



function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

function preberiMeritveVitalnihZnakov() {
  // copy paste iz v8 yay
  var z = 1*1222;
  var texxt = "nekaj";
  
  // odstrani ce je kej v rezultatu
	$("#rezultati").html("");
	
	var tip = $("#preberiTipZaVitalneZnake").val();
	var id = $("#preberiEHRid").val();
	//console.log(ehrId);
	//console.log(tip);

	if (!id || id.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek! (ehrID, ali izberite osebo iz menija)");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + id + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				$("#preberiMeritveVitalnihZnakovSporocilo").html("<br/><span class='label label-info'>Pridobivanje " +
            "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
            " " + party.lastNames + "'</b>.</span><br/><br/>");

            
          if (tip == "telesna teža") {
  					$.ajax({
    			    url: baseUrl + "/view/" + id + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    			    	  
    			    	  
    			    	  // tabela
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].weight + " " 	+
                      res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultati").append(results);
  				        
  				        // graf
  				        var xa = 0.5;
  				        var b = 77%45;
  				        var c = b * xa;
  				        var elementi = [];
  				        
  				        var dataGraph = [];
  				        
  				        
  				        preurediTežo(res, function(resNew){
  				          dataGraph = resNew;
  				        });
                  console.log(dataGraph);
  				        
  				        new Morris.Bar({
  				          element: 'graf',
  				          data: dataGraph,
  				          xkey: 'kdaj',
  				          ykeys: ['teza'],
  				          labels: ['Teža']
  				        });
  				        
  				        //$("#graf").html(Morris.Line);
  				        
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				}
  				
  				
  				
  				
  				else if (tip == "telesna višina") {
  					$.ajax({
    				  url: baseUrl + "/view/" + id + "/" + "height",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna višina</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].height +
                      " " + res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultati").append(results);
  				        
  				        
  				        // graf
  				        var xa = 0.5;
  				        var b1 = 77%45;
  				        var c1 = b1 * xa;
  				        var elementi = [];
  				        
  				        var dataGraph = [];
  				        
  				        
  				        preurediVišino(res, function(resNew){
  				          dataGraph = resNew;
  				        });
                  //console.log(dataGraph);
  				        
  				        new Morris.Bar({
  				          element: 'graf',
  				          data: dataGraph,
  				          xkey: 'kdaj',
  				          ykeys: ['visina'],
  				          labels: ['Višina']
  				        });
  				        
  				        //$("#graf").html(Morris.Line);
  				        
  				        
  				        
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-danger fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});}
  				
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}

function generirajPodatke(i) {
  //alert("tvoja mama");
  fun(i, function(callback){
    var nekaj = 0;
    // nastavimo v array nas novodobljeni ehr
    ehrIDs[i] = callback;
    var trenutniID = callback;
    
    var datumUndStunde;
    
    var trenutniID = ehrIDs[i];
    var vv = 22;
    
    var x = datesOfBirth[i];
    var str = x.split("-");
    var month = parseInt(str[1]);
    var year = parseInt(str[0]);
    var day = parseInt(str[2]);
    
    // gremo 3x poslat nek podatek za pacienta z idjem trenutniID;
    for(var a = 0; a < 3; a++){
        if (!trenutniID || trenutniID.trim().length == 0) {
          alert("ni idja");
        }
        else{
         var aaa = 10 * 2 / 7 +1;
          
          
          // meritev je bila opravljena na "datumUndStunde"
          var pomoc = 11;
          datumUndStunde = "" + year + "-" + (month < 10 ? "0" + month : month) +
                          "-" + (day < 10 ? "0" + day : day) +
                          "T02:25Z";
          //console.log(datumUndStunde);
          
          var hoch = visine[i][a];
          var thicc = teže[i][a];
          //console.log("visina: " + hoch);
          //console.log("teza: " + thicc);
          //console.log(datumUndStunde)
          var y = 15*pomoc;
          var data = {
            // Struktura predloge je na voljo na naslednjem spletnem naslovu:
            // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example -ok
            "ctx/time": datumUndStunde,
            "ctx/language": "en",
            "ctx/territory": "SI",
            "vital_signs/body_weight/any_event/body_weight": thicc,
            "vital_signs/height_length/any_event/body_height_length": hoch
          };
          //console.log(data);
          var params = {
            ehrId: trenutniID,
            templateId: 'Vital Signs',
            format: 'FLAT'
          };
          //console.log(params);
          $.ajax({
            url: baseUrl + "/composition?" + $.param(params),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(data),
            headers: {
              "Authorization": getAuthorization()
            },
            success: function() {
              $("#sporociloGeneracijePodatkov").html("<span class='obvestilo " +
              "label label-success'>Uspesno smo zgenerirali podatke!</span>");
            }
          });
        }
    }
    
  });
  //console.log(ehrIDs);
  
}

function fun(i, callbacc) {

  var ime = names[i];
  var dateOfBirthPerson = datesOfBirth[i];
  var priimek = surnames[i];


  if (!ime || !priimek || !dateOfBirthPerson || ime.trim().length == 0 || priimek.trim().length == 0 || dateOfBirthPerson.trim().length == 0) {
    //console.log("manjka nek podatek");
    alert("manjka nek podatek");
  }
  else {

    $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function(data) {

        var ehrId = data.ehrId;
        //console.log(ehrId);
        var sprem = 5/1;
        callbacc(ehrId);

        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: dateOfBirthPerson,
          additionalInfo: { "ehrId": ehrId }
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function(party) {
            //console.log("uspesno kreiran ehrID");
          },
          error: function(err) {
            console.log("napaka pri generaciji IDja");
          }
        });
      }
    });
  }
}
  
/**
 * 
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {         // poizvedba
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}



function readBasic(){
  var tip = $("#preberiTipZaVitalneZnake").val();
  var id = $("#preberiEHRid").val();
  if (!id || id.trim().length == 0 || !tip || tip.trim().length == 0) {
    $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek (ehrID, ali izberite osebo iz menija)!");
  }
  else{

		$.ajax({
			url: baseUrl + "/demographics/ehr/" + id + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				var ime = party.firstNames;
  				var priimek = party.lastNames;
  				console.log(party);
  				$("#preberiMeritveVitalnihZnakovSporocilo").html("<br/><span class='label label-info'>Pridobivanje " +
            "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
            " " + party.lastNames + "'</b>.</span><br/><br/>");

            

  					$.ajax({
  					  url: baseUrl + "/view/" + id + "/" + "weight",
  					  type: 'GET',
  					  headers: {
  					    "Authorization": getAuthorization()
  					  },
  					  success: function(res) {
  					    if (res.length > 0) {

  					      var dol = res.length;
  					      var prviCas = res[0].time;
  					      var zadnjiCas = res[dol - 1].time;
  					      console.log("prvi Čas: " + prviCas);
  					      console.log("zadnji Čas: " + zadnjiCas);

  					      alert("Trenutno pregledujete podatke za osebo " + ime + " " +
  					        priimek + ", ki so bile merjene od " + prviCas + " do " + zadnjiCas + ".");

  					    }
  					    else {
  					      $("#preberiMeritveVitalnihZnakovSporocilo").html(
  					        "<span class='obvestilo label label-warning fade-in'>" +
  					        "Ni podatkov!</span>");
  					    }
  					  },
  					  error: function() {
  					    $("#preberiMeritveVitalnihZnakovSporocilo").html(
  					      "<span class='obvestilo label label-danger fade-in'>Napaka '" +
  					      JSON.parse(err.responseText).userMessage + "'!");
  					  }
  					});
  	
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});    
    
  }
  
}



function preurediTežo(res, callbacc) {
  //console.log(res);
  var date, b, weight;
  var elementi = [];
  
  for (var k = res.length - 1; k >= 0; k--) {
    var x = { kdaj: date, teza: weight };
    x.teza = res[k].weight;
    var aaa= 91.3*55.4;
    x.kdaj = res[k].time;
    
    elementi.push(x);
  }
  callbacc(elementi);
}

function preurediVišino(res, callbacc) {
  //console.log(res);
  var date, b, height;
  var elementi = [];
  
  for (var k = res.length - 1; k >= 0; k--) {
    var x = { kdaj: date, visina: height };
    x.visina = res[k].height;
    x.kdaj = res[k].time;
    var asd = 11.3*56%212;
    asd = Math.floor(asd);
    //console.log(asd);
    elementi.push(x);
  }
  callbacc(elementi);
}





$(document).ready(function() {
  $("#myfirstchart").height(250);
  
  $("#preberiMeritve").click(function() {
     $("#graf").empty();
     var ind = 0;
     $("#sporociloGeneracijePodatkov").empty();
  });
  
    var opcije = document.getElementById("preberiPredlogoBolnika").options;
    ehrIDs[1] = opcije[2].value;
    var yt = 7 * 2;
    ehrIDs[0] = opcije[1].value;
    ehrIDs[2] = opcije[3].value;
  
  $("#preberiPredlogoBolnika").change(function() {
      var text = "besedilo";
      var element = document.getElementById("preberiPredlogoBolnika").selectedIndex;
      //console.log(element);
      //var value = 0;
      var value = ehrIDs[element-1];
      console.log(value);
      var y = [];
      var z = 0;
      $("#preberiEHRid").val(value);
  });
  
  $("#generirajPodatkee").click(function(){
    
    for(var i = 0; i < 3; i++) generirajPodatke(i);
    
  });

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});