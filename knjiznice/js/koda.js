/* global L, distance */

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

var mapa;
var bolnice = [];
var markeri = []
const Y1 = 14.518948;
const X1 = 46.061082;
var lavbicLink = "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json";

window.addEventListener('load', function() {
  console.log("nalozeno");
  var mapOptions = {
    center: [X1, Y1],
    zoom: 15
  };
  mapa = new L.map('mapa_id', mapOptions);

  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  mapa.addLayer(layer);

  var popup = L.popup();

  narMark();
  narBol(false);



  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
    popup
      .setLatLng(latlng)
      .setContent("Izbrana točka:" + latlng.toString())
      .openOn(mapa);

    narBol(true, latlng);
  }

  // kliknemo na mapo in se izvede funkcija višje
  mapa.on('click', obKlikuNaMapo);

});



function narMark() {
  var variable = 0;
  getData(function(jsonRezultat) {


    for (var i = 1; i < markeri.length; i++) {
      mapa.removeLayer(markeri[i]);
    }

    for (var i = 0; i < jsonRezultat.length; i++) {

      var vmes = jsonRezultat[i];
      if (vmes.geometry.type == "Point") {
        // obratno nastavimo vrednosti koordinat
        var lat = vmes.geometry.coordinates[0];
        var lng = vmes.geometry.coordinates[1];
        var rez = Math.floor(7*7);
        //console.log(rez);
        addMrk(lng, lat, vmes);
      }
    }
  });
}


function addMrk(lat, lng, jsonRezultat) {
  // copy pasta :3 iz dn1
  var ikona = new L.Icon({
    shadowUrl: 'http://teaching.lavbic.net/cdn/OIS/DN1/marker-shadow.png',
    iconUrl: 'http://teaching.lavbic.net/cdn/OIS/DN1/marker-icon-2x-red.png',
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    iconSize: [25, 41],
    
    shadowSize: [41, 41]
  });

  var marker = L.marker([lat, lng], { icon: ikona });
  var index = 0;
  var naziv = jsonRezultat.properties.name;

  var id = jsonRezultat.properties["@id"];
  var amenity = jsonRezultat.properties.amenity;
  var building = jsonRezultat.properties.building;



  marker.bindPopup(
    (naziv != null ? naziv + '<br>' : '') +
    (amenity != null ? (building != null ? ': ' : '') + 'je objekt<br>' : '<br>') +
    "id zgradbe je: " + id
  );
  markeri.push(marker);
  marker.addTo(mapa);

}

function getData(callback) {

  // copy paste iz V8
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", lavbicLink, true);
  xobj.onreadystatechange = function() {


    if (xobj.readyState == 4 && xobj.status == "200") {
      var json = JSON.parse(xobj.responseText);
      callback(json.features);
    }
  };
  xobj.send(null);
}

function narBol(bool, clicc) {
  getData(function(jsonRezultat) {

    for (var i = 0; i < bolnice.length; i++) {
      mapa.removeLayer(bolnice[i]);
    } // zbrisemo vse extra bolnice ki obstajajo

    for (var i = 0; i < jsonRezultat.length; i++) {
      var rez = jsonRezultat[i].geometry.type;
      var stevec = 0;
      var varr;

      if (rez == "Polygon") {
        var coors = jsonRezultat[i].geometry.coordinates[0];
        var xyzCoor;
        //coors.reverse();

        for (var k = 0; k < coors.length; k++) {
          coors[k].reverse();

        }
        var barva = 'blue';
        if (bool) {

          var x1 = clicc.lat;
          var y1 = clicc.lng;
          var y2;
          var x2;

          if (rez == "LineString") {
            y2 = coors[1];
            x2 = coors[0];



          }
          else if (rez == "Polygon") {
            y2 = coors[0][1];
            x2 = coors[0][0];
          }

          var dist = distance(x1, y1, x2, y2, "K");


          if (dist <= 3.5) {
            barva = 'green';
          }

        }

        var polygon = new L.polygon(coors, {
          color: barva,
          opacity: 1,
          fillColor: barva
        });

        var val = jsonRezultat[i].properties;

        var naziv = val.name;
        var amenity = val.amenity;

        var id = val["@id"];
        //console.log(id);


        // Izpišemo želeno sporočilo v oblaček
        // (naziv != null ? true : false)        

        polygon.bindPopup(

          (naziv != null ? naziv + '<br>' : '') +
          (amenity != null ? 'je objekt<br>' : '<br>') +
          "id zgradbe je: " + id
        );

        polygon.addTo(mapa);
        bolnice.push(polygon);
        //mapa.removeLayer(polygon);

      }
    }
  });

}



